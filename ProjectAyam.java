import java.util.Scanner;
public class ProjectAyam {

    public static Scanner input = new Scanner(System.in);
    static int N = 0, M = 0, Result = 0, batasBawah = 1, batasAtas = 1000000;

    static String dash = "================================", Nname = "Jumlah Ayam(N)", Mname = "batas Jumlah ayam tiap kandang(M)", limit = " yang anda masukkan tidak memenuhi syarat", error = "", Dash = "-------------------------------------------------";
    static String namaAplikasi = "Penghitung jumlah kandang", function = "Aplikasi ini akan menghitung jumlah kandang yang di butuhkan\n" + "Dengan meng-input " + Nname + " dan " + Mname + "\n" + Dash + "\nDengan Syarat : \nNilai N dan M berupa bilangan bulat dari " + batasBawah + "-" + batasAtas;
    static boolean SalahDuaDuanya = false;

    public static void main(String[] args) {
        System.out.println("Selamat Datang di Aplikasi " + namaAplikasi);

//Start of Optional

        System.out.println(function);

//End of Optional
        System.out.println(dash);
        System.out.print("Masukkan " + Nname + " dan " + Mname);
        System.out.println(" dipisahkan dengan spasi \n" + Dash);
        ProjectAyam.Start();
        ProjectAyam.Nilai();
    }

    public static void Start() {
        error = null;
        SalahDuaDuanya = false;
//--------------------------------------------------
        System.out.print("Input (N M) : ");
        N = input.nextInt();
        M = input.nextInt();
        System.out.println(dash);
        //Start of Input Check
        if ((N < batasBawah || N > batasAtas)) {
//            ProjectAyam.ErrorN();
            error = Nname;
            SalahDuaDuanya = true;
        }
        if (M < batasBawah || M > batasAtas) {
            if (SalahDuaDuanya == true) {
                error = Nname + " dan " + Mname;
            } else {
//            ProjectAyam.ErrorM();
                error = Mname;
            }
        }
        ProjectAyam.CheckError();
    }

    static void Nilai() {
        if (N % M > 0) {
            Result = (int) N / M + 1;
        } else {
            Result = (int) N / M;
        }

        System.out.println("Jumlah Kandang yang dibutuhkan : " + Result + " kandang");
        System.out.println(dash);
    }

    static void CheckError() {
        if (error != null) {
            System.out.println(error + limit);
            System.out.println(dash);
            ProjectAyam.Start();
        }
    }
}